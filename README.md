# Earth Clock

A map of Earth with the position of the Sun.

#### Clone Repo

    git clone git@gitlab.com:robertmermet/earth-clock.git

#### View Demo

>**demo** [robertmermet.com/projects/earth-clock](http://robertmermet.com/projects/earth-clock)

![Wernher von Braun's headstone](./WernherVonBraun.jpg)

> "The heavens declare the glory of God; and the firmament sheweth his handywork."

Psalm 19:1